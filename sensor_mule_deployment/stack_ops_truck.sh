#!/bin/bash


partition_and_format() {
    local DRIVE=$1
    local TARGET_PARTITION=${DRIVE}1 # Partition should be 1
    local PARTITION="${2:-false}"
    local FORMAT="${3:-$PARTITION}"
    local TUNE2FS="${4:-$FORMAT}"

    # Check if DRIVE is set, and is found by lsblk
    lsblk -lp | grep -q ${DRIVE}
    if [ $? -eq 1 ]; then
        echo "${DRIVE} not attached, can't continue"
        return 1
    fi
    if [ $PARTITION = "true" ]; then
        sudo sfdisk -q -w always -W always ${DRIVE} <<EOF
label: gpt
;
EOF
2>&1 > /dev/null
        if [ $? -eq 1 ]; then
            echo "Partitioning ${DRIVE} failed, can't continue"
            return 1
        fi
    fi
    if [ $FORMAT = "true" ]; then
        sudo lsblk -lp | grep -q ${TARGET_PARTITION}
        if [ $? -eq 1 ]; then
            echo "Partition ${TARGET_PARTITION} not found after partitioning, can't continue"
            return 1
        fi
        # Unfortunately there's no parameter I can pass mkfs.ext4 to tell it to force, so we send it "yes, do it"
        yes | sudo mkfs -t ext4 -E discard ${TARGET_PARTITION} > /dev/null
        if [ $? -eq 1 ]; then
            echo "Format of partition ${TARGET_PARTITION} failed, can't continue"
            return 1
        fi
    fi
    if [ $TUNE2FS = "true" ]; then
        # According to this article we don't need the defaults of 5%: https://superuser.com/questions/1256074/how-much-space-to-leave-free-on-hdd-or-ssd/1257550#1257550
        echo "Using tune2fs to change the amount of blocks held in reserve for root"
        sudo tune2fs ${TARGET_PARTITION} -r 256000 2>&1 > /dev/null # This reserves a fixed 1GB, we can always change this later
        if [ $? -eq 1 ]; then
            echo "Could not call tune2fs on ${TARGET_PARTITION}, can't continue"
            return 1
        fi
    fi
}

LOGGING_DIRECTORY=/media/sda1
LOGGING_PARTITION=/dev/disk/by-path/platform-3610000.xhci-usb-0:4:1.0-scsi-0:0:0:0-part1
LOGGING_DRIVE=/dev/disk/by-path/platform-3610000.xhci-usb-0:4:1.0-scsi-0:0:0:0
# Step 1: see if the logging drive is mounted
if ! $(findmnt ${LOGGING_DIRECTORY} > /dev/null); then
    echo "${LOGGING_DIRECTORY} is NOT mounted, checking if drive exists"
    lsblk -lp | grep -q ${LOGGING_DRIVE}
    if [ $? -eq 1 ]; then
        echo "Logging drive not attached, please attach and rerun script"
        exit -1
    else
        echo "${LOGGING_DRIVE} found, checking if partitioned"
        lsblk -lp | grep -q ${LOGGING_PARTITION}
        if [ $? -eq 1 ]; then
            echo "${LOGGING_PARTITION} not partitioned, partioning"
            partition_and_format ${LOGGING_DRIVE} "true" "true" "false"
            if [ $? -eq 1 ]; then
                echo "Error when preparing ${LOGGING_DRIVE}, can't continue"
                exit -1
            fi
        fi
    fi
    echo "Mounting logging partition"
    sudo mount ${LOGGING_PARTITION} ${LOGGING_DIRECTORY}
    if [ $? -eq 32 ]; then
        echo "Error when mounting ${LOGGING_PARTITION} to ${LOGGING_DIRECTORY}, can't continue"
        exit -1
    fi
fi

DONT_DELETE=device_label.bin

# Step 2: clear the logging drive of all files that aren't device_label.bin
echo "Clearing existing data from logging drive"
find ${LOGGING_DIRECTORY} ! -wholename ${LOGGING_DIRECTORY} -a ! -name ${DONT_DELETE} -exec rm -rf {} \;

# Step 3: Creating logs folder if it doesn't exist, or empty it if it does already exist
LOGS_DIR=~/logs
if ! [ -d $LOGS_DIR ]
then
    echo "Creating ${LOGS_DIR}"
    mkdir -p ${LOGS_DIR}
else
    echo "Removing old files from ${LOGS_DIR}"
    find ${LOGS_DIR} ! -wholename ${LOGS_DIR} -exec rm -rf {} \;
fi

# Step 4: Configure the lidar
NET_DEVICE=mgb3_0
IP_ADDR=192.168.1.112
ip -br addr show | grep -q ${NET_DEVICE}
if [ $? -eq 1 ]
then
    echo "${NET_DEVICE} does not exist, aborting"
    exit -1
fi

ip -br addr show ${NET_DEVICE} | grep -q ${IP_ADDR}
if [ $? -eq 1 ]
then
    echo "Configuring lidar device"
    if [ ! -f  ./sensor_mule_deployment/configure ]
    then
        echo "./sensor_mule_deployment/configure does not exist, aborting"
        exit -1
    fi
    ./sensor_mule_deployment/configure
    ip -br addr show ${NET_DEVICE} | grep ${IP_ADDR}
    if [ $? -eq 1 ]
    then
        echo "Failed to configure lidar device"
        exit -1
    else
        echo "Lidar successfully configured"
    fi
fi
