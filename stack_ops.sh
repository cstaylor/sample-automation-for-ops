#!/bin/bash
SSH_HOST=vdi
TARGET_DIR=sensor_mule_deployment

# Step 1: Get everything to the server, moving the old
# directory to a different location first if it is found
ssh -q $SSH_HOST [ -d $TARGET_DIR ]
if [ $? -eq 0 ]
then
    BACKUP_DIR=${TARGET_DIR}_$(date +'%Y_%m_%d')
    echo "Renaming: ${TARGET_DIR} to ${BACKUP_DIR}"
    ssh -q $SSH_HOST mv ${TARGET_DIR} ${BACKUP_DIR}
fi
echo "Copying data to server"
scp -r ${TARGET_DIR} $SSH_HOST:

# Step 2: ssh into the destination and then execute the test script there
ssh -q $SSH_HOST ./sensor_mule_deployment/stack_ops_truck.sh

